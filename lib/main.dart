import 'dart:ui';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/services.dart';
///-----------------------------------------------------------------------------
void main() => runApp(MyApp());

///Global Songtitle
String songtitle ='';
bool vtylerpink = false;
bool vtylerorange = false;
bool vbeatles = false;
bool vbeyonce = false;
bool vcoldplay = false;
bool vja = false;
bool vkhalid = false;
bool vlb = false;
bool vnirvana = false;
bool vpf = false;
bool vcg = false;

bool isSelected1 = true;
bool isSelected2 = true;
bool isSelected3 = true;
bool isSelected4 = true;


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 4',
      home: HomePage(),
    );
  }
}
///this is america
///HomePage Class
///-----------------------------------------------------------------------------
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ///Audio Clips mp3
  ///---------------------------------------------------------------------------
  static AudioCache cache = AudioCache();
  AudioPlayer player;

  bool isPlaying = false;
  bool isPaused = false;

  void playHandler() async {
    if (isPlaying) {
      player.stop();
    } else {
      player = await cache.play(songtitle);
    }

    setState(() {
      if (isPaused) {
        isPlaying = false;
        isPaused = false;
      } else {
        isPlaying = !isPlaying;
      }
    });
  }

  void pauseHandler() {
    if (isPaused && isPlaying) {
      player.resume();
    } else {
      player.pause();
    }
    setState(() {
      isPaused = !isPaused;
    });
  }
  ///---------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: SideMenu(),
        appBar: AppBar(
        title: Text('Home'),
    backgroundColor: Colors.pink[600],
    ),
    backgroundColor: Colors.grey[900],
      body: GridView.count(
        primary: false,
        padding: const EdgeInsets.all(20),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        children: <Widget>[
          ///-------------------------------------------------------------------
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/beats.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/beat.jpg'),
                    fit: BoxFit.fill,
                    ),
                  ),
                ),
              ),
          ),
          Card(
              child: GestureDetector(
                onTap: () {setState(() {
                  songtitle = 'audio/partition.mp3';
                  playHandler();
                });},
                child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('images/beyon.jpg'),
                          fit: BoxFit.fill,
                        )
                    )
                ),
              )
          ),
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/cp.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/cp.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/jasom.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/ja.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/khalid.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/khalid.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/lbone.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/lb.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/teen.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/nirvana.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/tylerpink.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/tyler2.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/tylerorange.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/tyler.jpeg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/floyd.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/pf.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Card(
            child: GestureDetector(
              onTap: () {setState(() {
                songtitle = 'audio/cgamerica.mp3';
                playHandler();
              });},
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('images/america.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          ///-------------------------------------------------------------------
        ],
      ),
    );
  }
}

///Songs Class
///-----------------------------------------------------------------------------
class Songs extends StatefulWidget {
  @override
  _SongsState createState() => _SongsState();
}

enum Options {Add, Remove}

class _SongsState extends State<Songs> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void addSongToLibrary(String songName) {
    if(songName == 'Igor')
    vtylerpink = true;

    if(songName == 'Flower Boy')
      vtylerorange = true;

    if(songName == 'Money')
      vpf = true;

    if(songName == 'On My Way')
      vkhalid = true;

    if(songName == 'Smells Like Teen Spirit')
      vnirvana = true;

    if(songName == 'Sativa')
      vja = true;

    if(songName == 'Yellow')
      vcoldplay = true;

    if(songName == 'Partition')
      vbeyonce = true;

    if(songName == 'Here Comes the Sun')
      vbeatles = true;

    if(songName == 'One Margarita')
      vlb = true;

    if(songName == 'This Is America')
      vcg = true;
  }

  void removeSongFromLibrary(String songName) {
    if(songName == 'Igor')
      vtylerpink = false;

    if(songName == 'Flower Boy')
      vtylerorange = false;

    if(songName == 'Money')
      vpf = false;

    if(songName == 'On My Way')
      vkhalid = false;

    if(songName == 'Smells Like Teen Spirit')
      vnirvana = false;

    if(songName == 'Sativa')
      vja = false;

    if(songName == 'Yellow')
      vcoldplay = false;

    if(songName == 'Partition')
      vbeyonce = false;

    if(songName == 'Here Comes the Sun')
      vbeatles = false;

    if(songName == 'One Margarita')
      vlb = false;

    if(songName == 'This Is America')
      vcg = false;
  }

  PopupMenuButton determineString(String songName) {
    return PopupMenuButton<Options>(
        color: Colors.white,
        padding: EdgeInsets.all(2),
        onSelected: (Options result) {setState(() {
          _scaffoldKey.currentState.showSnackBar(
              SnackBar(
                content: result == Options.Add ? Text('Added to Library!') : Text('Removed from Library!'),
                duration: Duration(seconds: 1),
                backgroundColor: Colors.pink[600],
              ));
          _selection = result;
          if(result == Options.Add)
            addSongToLibrary(songName);
          if(result == Options.Remove)
            removeSongFromLibrary(songName);
        });},
        itemBuilder: (BuildContext context) => <PopupMenuEntry<Options>> [
          const PopupMenuItem<Options>(
            value: Options.Add,
            child: ListTile(
              leading: const Icon(Icons.add),
              title: Text('Add to Library'),
            ),
          ),
          const PopupMenuItem<Options>(
            value: Options.Remove,
            child: ListTile(
              leading: const Icon(Icons.remove),
              title: Text('Removed from Library'),
            ),
          ),
        ]
    );
  }

  Options _selection;
  
  ///Audio Clips mp3
  ///---------------------------------------------------------------------------
  static AudioCache cache = AudioCache();
  AudioPlayer player;

  bool isPlaying = false;
  bool isPaused = false;

  void playHandler() async {
    if (isPlaying) {
      player.stop();
    } else {
      player = await cache.play(songtitle);
    }

    setState(() {
      if (isPaused) {
        isPlaying = false;
        isPaused = false;
      } else {
        isPlaying = !isPlaying;
      }
    });
  }

  void pauseHandler() {
    if (isPaused && isPlaying) {
      player.resume();
    } else {
      player.pause();
    }
    setState(() {
      isPaused = !isPaused;
    });
  }
  ///---------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        drawer: SideMenu(),
        appBar: AppBar(
          title: Text('Songs'),
          backgroundColor: Colors.pink[600],
        ),
        backgroundColor: Colors.grey[900],
        body: ListView(
          children: <Widget>[
            ///-----------------------------------------------------------------
            Row(
              //mainAxisAlignment: MainAxisAlignment.,
              children: [
            SizedBox(
              width: 8,
            ),
            FilterChip(
              label: Text('Country'),
              labelStyle: TextStyle(color: isSelected1 ? Colors.black : Colors.black),
              selected: isSelected1,
              onSelected: (bool selected) {
                setState(() {
                  isSelected1 = !isSelected1;
                });
              },
              selectedColor: Colors.pink[600],
              checkmarkColor: Colors.black,
            ),
            FilterChip(
              label: Text('Pop Music'),
              labelStyle: TextStyle(color: isSelected2 ? Colors.black : Colors.black),
              selected: isSelected2,
              onSelected: (bool selected) {
                setState(() {
                  isSelected2 = !isSelected2;
                });
              },
              selectedColor: Colors.pink[600],
              checkmarkColor: Colors.black,
            ),
            FilterChip(
              label: Text('Rock'),
              labelStyle: TextStyle(color: isSelected3 ? Colors.black : Colors.black),
              selected: isSelected3,
              onSelected: (bool selected) {
                setState(() {
                  isSelected3 = !isSelected3;
                });
              },
              selectedColor: Colors.pink[600],
              checkmarkColor: Colors.black,
            ),
            FilterChip(
              label: Text('Rap'),
              labelStyle: TextStyle(color: isSelected1 ? Colors.black : Colors.black),
              selected: isSelected4,
              onSelected: (bool selected) {
                setState(() {
                  isSelected4 = !isSelected4;
                });
              },
              selectedColor: Colors.pink[600],
              checkmarkColor: Colors.black,
            ),
            ],),
            ///-----------------------------------------------------------------
            Visibility(
              visible: isSelected2,
              child: ListTile(
                leading: Image.asset('images/beat.jpg'),
                title: Text('Beatles - Here Comes the Sun ', style: TextStyle(color: Colors.white)),
                trailing: determineString('Here Comes the Sun'),
                onTap: () {setState(() {
                  songtitle = 'audio/beats.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: isSelected2,
              child: ListTile(
                leading: Image.asset('images/beyon.jpg'),
                title: Text('Beyonce - Partition', style: TextStyle(color: Colors.white)),
                trailing: determineString('Partition'),
                onTap: () {setState(() {
                  songtitle = 'audio/partition.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: isSelected2,
              child: ListTile(
                leading: Image.asset('images/cp.jpg'),
                title: Text('ColdPlay - Yellow', style: TextStyle(color: Colors.white)),
                trailing: determineString('Yellow'),
                onTap: () {setState(() {
                  songtitle = 'audio/cp.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: isSelected2,
              child: ListTile(
                leading: Image.asset('images/ja.png'),
                title: Text('Jhene Aiko - Sativa', style: TextStyle(color: Colors.white)),
                trailing: determineString('Sativa'),
                onTap: () {setState(() {
                  songtitle = 'audio/jasom.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: isSelected2,
              child: ListTile(
                leading: Image.asset('images/khalid.jpg'),
                title: Text('Khalid - On My Way', style: TextStyle(color: Colors.white)),
                trailing: determineString('On My Way'),
                onTap: () {setState(() {
                  songtitle = 'audio/khalid.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: isSelected1,
              child: ListTile(
                leading: Image.asset('images/lb.jpg'),
                title: Text('Luke Bryan - One Margarita', style: TextStyle(color: Colors.white)),
                trailing: determineString('One Margarita'),
                onTap: () {setState(() {
                  songtitle = 'audio/lbone.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: isSelected3,
              child: ListTile(
                leading: Image.asset('images/nirvana.jpg'),
                title: Text('Nirvana - Smells Like Teen Spirit', style: TextStyle(color: Colors.white)),
                trailing: determineString('Smells Like Teen Spirit'),
                onTap: () {setState(() {
                  songtitle = 'audio/teen.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: isSelected4,
              child: ListTile(
                leading: Image.asset('images/tyler.jpeg'),
                title: Text('Tyler the Creator - Flower Boy', style: TextStyle(color: Colors.white)),
                trailing: determineString('Flower Boy'),
                onTap: () {setState(() {
                  songtitle = 'audio/tylerorange.mp3';
                  playHandler();
                });},
              ),
            ),
            ///Tyler the Creator - Igor
            Visibility(
              visible: isSelected4,
              child: ListTile(
                leading: Image.asset('images/tyler2.png'),
                title: Text('Tyler the Creator - Igor', style: TextStyle(color: Colors.white)),
                trailing: determineString('Igor'),
                onTap: () {setState(() {
                  songtitle = 'audio/tylerpink.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: isSelected3,
              child: ListTile(
                leading: Image.asset('images/pf.png'),
                title: Text('Pink Floyd - Money', style: TextStyle(color: Colors.white)),
                trailing: determineString('Money'),
                onTap: () {setState(() {
                  songtitle = 'audio/floyd.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: isSelected4,
              child: ListTile(
                leading: Image.asset('images/america.jpg'),
                title: Text('Childish Gambino - This Is America', style: TextStyle(color: Colors.white)),
                trailing: determineString('This Is America'),
                onTap: () {setState(() {
                  songtitle = 'audio/cgamerica.mp3';
                  playHandler();
                });},
              ),
            ),

            ///-----------------------------------------------------------------
          ],
        )
    );
  }
}

class Genres extends StatefulWidget {
  @override
  _GenresState createState() => _GenresState();
}

class _GenresState extends State<Genres> {

  ///Audio Clips mp3
  ///---------------------------------------------------------------------------
  static AudioCache cache = AudioCache();
  AudioPlayer player;

  bool isPlaying = false;
  bool isPaused = false;

  void playHandler() async {
    if (isPlaying) {
      player.stop();
    } else {
      player = await cache.play(songtitle);
    }

    setState(() {
      if (isPaused) {
        isPlaying = false;
        isPaused = false;
      } else {
        isPlaying = !isPlaying;
      }
    });
  }

  void pauseHandler() {
    if (isPaused && isPlaying) {
      player.resume();
    } else {
      player.pause();
    }
    setState(() {
      isPaused = !isPaused;
    });
  }
  ///---------------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: SideMenu(),
        appBar: AppBar(
          title: Text('Your Library'),
          backgroundColor: Colors.pink[600],
        ),
      backgroundColor: Colors.grey[900],
        body: ListView(
          children: [
            Visibility(
              visible: vbeatles,
              child: ListTile(
                leading: Image.asset('images/beat.jpg'),
                title: Text('Beatles - Here Comes the Sun ', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/beats.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: vbeyonce,
              child: ListTile(
                leading: Image.asset('images/beyon.jpg'),
                title: Text('Beyonce - Partition', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/partition.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: vcoldplay,
              child: ListTile(
                leading: Image.asset('images/cp.jpg'),
                title: Text('ColdPlay - Yellow', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/cp.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: vja,
              child: ListTile(
                leading: Image.asset('images/ja.png'),
                title: Text('Jhene Aiko - Sativa', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/jasom.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: vkhalid,
              child: ListTile(
                leading: Image.asset('images/khalid.jpg'),
                title: Text('Khalid - On My Way', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/khalid.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: vlb,
              child: ListTile(
                leading: Image.asset('images/lb.jpg'),
                title: Text('Luke Bryan - One Margarita', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/lbone.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: vnirvana,
              child: ListTile(
                leading: Image.asset('images/nirvana.jpg'),
                title: Text('Nirvana - Smells Like Teen Spirit', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/teen.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: vtylerorange,
              child: ListTile(
                leading: Image.asset('images/tyler.jpeg'),
                title: Text('Tyler the Creator - Flower Boy', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/tylerorange.mp3';
                  playHandler();
                });},
              ),
            ),
            ///Tyler the Creator - Igor
            Visibility(
              visible: vtylerpink,
              child: ListTile(
                leading: Image.asset('images/tyler2.png'),
                title: Text('Tyler the Creator - Igor', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/tylerpink.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: vpf,
              child: ListTile(
                leading: Image.asset('images/pf.png'),
                title: Text('Pink Floyd - Money', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/floyd.mp3';
                  playHandler();
                });},
              ),
            ),
            Visibility(
              visible: vcg,
              child: ListTile(
                leading: Image.asset('images/america.jpg'),
                title: Text('Childish Gambino - This Is America', style: TextStyle(color: Colors.white)),
                onTap: () {setState(() {
                  songtitle = 'audio/cgamerica.mp3';
                  playHandler();
                });},
              ),
            ),
          ],
        ),
    );
  }
}

///SideMenu Class
///-----------------------------------------------------------------------------
class SideMenu extends StatefulWidget {
  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  void openHomePage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  void openSongsPage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Songs()));
  }

  void openGenresPage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Genres()));
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Navigation Menu', style: TextStyle(color: Colors.white, fontSize: 25),),
            decoration: BoxDecoration(
                image: DecorationImage(fit: BoxFit.fill, image: AssetImage('images/party.jpg'))),),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () {openHomePage();},
          ),
          ListTile(
            leading: Icon(Icons.music_note),
            title: Text('Songs'),
            onTap: () {openSongsPage();},
          ),
          ListTile(
            leading: Icon(Icons.library_books),
            title: Text('Your Library'),
            onTap:  () {openGenresPage();},
          ),
        ],
      ),
    );
  }
}